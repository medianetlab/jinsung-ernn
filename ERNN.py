import scipy.io as sio
import pandas as pd
import numpy as np
import tensorflow as tf
import tflearn
import sklearn
from sklearn import metrics



def RNN_A_3DRNN(Train_Feature, Test_Feature, Train_Label, Test_Label):
    # Data Input
    
    # RNN with tflearn
    # Initialization
    tf.reset_default_graph()
    # Shape of the input
    [samples, time, features] = Train_Feature.shape

    # Build the networks
    # 1. Input layer
    net = tflearn.input_data([None, time, features])
    # 2. Bi-directional RNN layer
    net = tflearn.bidirectional_rnn(net, tflearn.BasicRNNCell(features * 2, activation = 'tanh',weights_init = 'xavier'),
                                    tflearn.BasicRNNCell(features * 2, activation = 'tanh',weights_init = 'xavier'),
                                    return_seq = True, dynamic=True)
    net = tf.stack(net, axis = 1)
    # 3. Propagate RNN layer
    net = tflearn.simple_rnn(net, features * 2, activation='tanh', return_seq = False, dynamic=True, weights_init = 'xavier')
    # 4. FC layer
    net = tflearn.fully_connected(net, 1, activation='linear')
    # 5. DNN Model
    net = tflearn.regression(net, optimizer='adam', learning_rate=0.01, loss='mean_square')
    model = tflearn.DNN(net, tensorboard_verbose=0)

    # 6. Train
    model.fit(Train_Feature, Train_Label, n_epoch =100, show_metric=False, batch_size=100, shuffle=False)

    # 7. Test
    test_prediction = np.array(model.predict(Test_Feature))

    # 8. Performance Evaluation
    auc_fpr = sklearn.metrics.mean_squared_error(Test_Label, test_prediction)

    print("Performance is {}".format(auc_fpr))
    
    return [auc_fpr]
    